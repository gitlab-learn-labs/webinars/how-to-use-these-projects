This document provides a recommended template and sections that should be included in a readme file for any project contributed to the shared demo space. The purpose of providing this common readme is to ensure that contributed projects have enough information available to help consumers of the project understand the functionality and sample stories related to the project.  Additionally, as the projects are utilized for demonstrations, recordings can be attached to the readme as well. 

# Project Overview

Provide a brief overview of the project including the related language(s) and target functionality or flows. If the project was forked from another source, feel free to include that source to give credit where it is due and potentially help with the evolution. 

## The Story

This section provides a sample story or scenario that can be used to help set up the purpose of the project. This could be as simple as the customer scenario or situation that prompted the creation of this project. By understanding the motivating story it can help us determine if the goal of this particular project is a good fit. 

### Challenges

This section should include a bulleted list of the challenges and implications that exist as a result of the story. Essentially, why does the functionality or workflow related to this project matter? 

List of challenges:
* One
* Two

What these challenges mean to the customer:
* One'
* Two'

## How to use this Project

Any recommendation of demonstration flow and "gotchas" can be included here. Important things to highlight for the demonstration as well as conversations or topics to try to avoid. 

## Recordings
|Date Provided|Recording Link|Password|
|----|----|---|
|||