# First Ask


Before you do anything else, please go ahead and request access [here](https://gitlab.com/gitlab-learn-labs/webinars) by clicking the three dots in the top right hand corner and then **Request access**. Once requested an existing memeber of the group will give you dev access. That will ensure you are able to access all of the **private content** and can help contribute in the future.


# How to use Demo Projects


**Please do not use these projects as your own demo project outside of helping to contribute**


First and foremost this shared project initiative is most successful when we all take part in GitLab’s value of Collaboration. When using the projects if you come across a bug or missing project you think would have value please create an issue on the [Dev Issue Board](https://gitlab.com/gitlab-learn-labs/webinars/dev-issue-board). There is a whole team of people who will take on the issue to fix said project or create a new demo, but that can only happen if they are made aware.


These projects will be kept up to date with the latest features and we encourage you to help contribute as well. The general assumption is that you have already set up your own ultimate group and gcp project to fork these projects into. If you have not set up your space yet, check out this readme on how to get set up on the demo systems: https://gitlab.com/gitlab-com/customer-success/demo-engineering/demo-systems-initial-set-up


You can use the group profile at the top level of the group to help find the project you are looking for. Each project in the group should be listed along with a short description on what the project covers and a link to the project. If you use the built in GitLab navigation you will also be able to see labels that indicate the project was recently added or updated, or even if they have won the best project monthly contest in the past (More on that below).

Once in the project it's up to the original contributor but there should either be a verbose script on how one may give a demo or linked recordings on how best to give the presentation. At that point you should have all the tools you need to successfully run the demo.






# Weekly Office Hours


Join our weekly office hours to learn about enhancements to the group, ask/answer any demo related questions, or request a creation of a demo here: https://docs.google.com/document/d/1u8e-iElGfXvVquIAgJwRO6hwWTgd0hk7UBkDOD-8zQc/edit


# Give Back to the Community


Please let whomever is currently keeping a project up to date know you are thankful! Take advantage of the star functionality for projects that have helped you so others know which is the best demo per category. Other ways you can give back are the thanks channel, leaving comments etc it encourages other devs to continue to keep this group going.


# Monthly Best Project Contribution Contest


Once a month the weekly office hours call is dedicated to identifying a standout project that was added to the CS Shared Demo space to win best project of the month. All projects are judged on their title, description, quality of script or recording, and number of people using the group and giving kudos.


# How to Fork & Mirror


Fork and mirror if your main goal is to stay up to date with the project. Sometimes it is best to just fork the project and not mirror if you are afraid of upstream changes being pushed out or want to customize the project further.


1. First decide which of the projects in the ***Webinars & Demos*** group you want to use and click into it.
2. Once decided, on the project main page click **Fork** and fork the project into your personal ultimate group.If you don't have your ultimate group set up yet, go here: https://gitlab.com/gitlab-com/customer-success/demo-engineering/demo-systems-initial-set-up and follow the first set of instructions to do so.
3. Once forked go back to the project main screen and click **Clone** then click the copy option under **Clone with HTTPS** that will get us the .git url we need to mirror.
4. Now go back to your forked project and click through **Settings > General > Advanced** then scroll down to the ***Remove fork relationship*** section and click **Remove fork relationship**.
5. Next we want to use the left hand navigation menu again to click through **Settings > Repository** and expand the ***Mirroring repositories*** section.
6. In the ***Git repository URL*** section paste the .git URL we grabbed earlier, ensure ***Mirror direction*** is set to pull, then click **Mirror repository**
7. Go back to the main page of your imported project to make sure it doesn't have any broken mirror errors listed.


# Shared Infrastructure


The demo team also provides a set of shared Infrastructure accessible [here](https://gitlab.com/gitlab-learn-labs/webinars/demo-engineering-shared-infra). You can request access, changes, or additional infra in the access requests linked there.


## How to Contribute As a Dev
If you want to contribute feel free to request developer access to the group and you can be added to start helping improve demo projects that many others are using. Please keep in mind that if you get dev access these projects are not for you to start personalizing for an upcoming demo with company xyz, but rather if you want to add a new feature, improve the suggested demos issue, or fix a bug that you came across (as long as it's a bug that isn't meant to be there). We have a ***Demo Architect Issue Board*** project where you can pick up tasks that others have requested or request your own. If you add your own projects, please make sure to add the project to our internal stack overflow (https://stackoverflowteams.com/c/gitlab-customer-success/questions) as well so others can easily find it!


## Contribution Guidelines
* Make it public
* Good, descriptive name
* Script/video
* Add to project profile page
* Understand that you will have issues assigned if stale for more than 31 days
* Label will be added for untouched after one year

Example project: https://gitlab.com/gitlab-learn-labs/webinars/ai/code-suggestions-python-demo



## How to Contribute As a User
You can also contribute by opening bug issues against a project or adding suggestions for future content for other Devs to pick up. If you also notice a missing demo that would be useful, open an issue and someone can pick it up. Check out the open issues here: https://gitlab.com/gitlab-learn-labs/webinars/how-to-use-these-projects/-/boards


Feel free to reach out to Logan Stucker with any questions
